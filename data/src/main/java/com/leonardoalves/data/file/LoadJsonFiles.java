package com.leonardoalves.data.file;

import android.content.Context;
import android.content.res.Resources;

import com.google.gson.stream.JsonReader;
import com.leonardoalves.data.R;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.InputStream;

/**
 * Created by leonardo on 04/04/17.
 */

public class LoadJsonFiles {
    public InputStream loadFromFile(Context context, String fileName){
        int id = context.getResources().getIdentifier(fileName, "raw", context.getPackageName());
        InputStream inputStream = context.getResources().openRawResource(id);
        return inputStream;
    }
}
