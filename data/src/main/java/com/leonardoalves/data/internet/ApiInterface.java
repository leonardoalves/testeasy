package com.leonardoalves.data.internet;

import com.leonardoalves.data.form.entities.CellsEntity;
import com.leonardoalves.data.investiment.entities.Investiment;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by leonardo on 01/04/2017.
 */

public interface ApiInterface {
    @GET("json.json")
    Call<CellsEntity> getFields();

    @GET("fund.json")
    Call<Investiment> getScreen();
}
