package com.leonardoalves.data.form.entities;

import com.google.gson.annotations.SerializedName;
import com.leonardoalves.data.form.enums.FieldType;
import com.leonardoalves.data.form.enums.FillTypeField;

/**
 * Created by leonardo on 01/04/2017.
 */

public class FieldEntity {
    @SerializedName("id")
    private Integer id;
    @SerializedName("type")
    private String type;
    @SerializedName("message")
    private String message;
    @SerializedName("typefield")
    private String typefield;
    @SerializedName("hidden")
    private Boolean hidden;
    @SerializedName("topSpacing")
    private Double topSpacing;
    @SerializedName("show")
    private String show;
    @SerializedName("required")
    private Boolean required;

    private FieldType viewType;
    private FillTypeField fillTypeField;

    public FieldEntity(Integer id, String type, String message, String typefield, Boolean hidden, Double topSpacing, String show, Boolean required) {
        this.id = id;
        this.type = type;
        this.message = message;
        this.typefield = typefield;
        this.hidden = hidden;
        this.topSpacing = topSpacing;
        this.show = show;
        this.required = required;

        this.viewType = FieldType.fromString(type);
        this.fillTypeField = FillTypeField.fromString(typefield);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTypefield() {
        return typefield;
    }

    public void setTypefield(String typefield) {
        this.typefield = typefield;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public Double getTopSpacing() {
        return topSpacing;
    }

    public void setTopSpacing(Double topSpacing) {
        this.topSpacing = topSpacing;
    }

    public String getShow() {
        return show;
    }

    public void setShow(String show) {
        this.show = show;
    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }

    public FieldType getViewType() {
        if (viewType == null){
            this.viewType = FieldType.fromString(type);
        }
        return viewType;
    }

    public FillTypeField getFillTypeField() {
        if (fillTypeField == null){
            this.fillTypeField = FillTypeField.fromString(typefield);
        }
        return fillTypeField;
    }
}
