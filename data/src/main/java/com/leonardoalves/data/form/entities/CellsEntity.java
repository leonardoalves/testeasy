package com.leonardoalves.data.form.entities;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CellsEntity {
    @SerializedName("cells")
    private List<FieldEntity> cells;

    public CellsEntity(List<FieldEntity> cells) {
        this.cells = cells;
    }

    public List<FieldEntity> getCells() {
        return cells;
    }

    public void setCells(List<FieldEntity> cells) {
        this.cells = cells;
    }
}
