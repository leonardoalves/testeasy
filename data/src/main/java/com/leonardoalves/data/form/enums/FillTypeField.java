package com.leonardoalves.data.form.enums;

/**
 * Created by leonardo on 01/04/2017.
 */

public enum  FillTypeField {
    text("1"),
    telNumber("2"),
    email("3");

    private final String name;

    FillTypeField(String s) {
        name = s;
    }

    public boolean equalsName(String otherName) {
        return name.equals(otherName);
    }

    public String toString() {
        return this.name;
    }

    public static FillTypeField fromString(String name) {
        try {
            return findByName(name);
        } catch (Exception e) {
            return null;
        }
    }

    public static FillTypeField findByName(String name) throws Exception {
        for (FillTypeField p : values()) {
            if (name.equals(p.toString())) {
                return p;
            }
        }
        return null;
    }
}
