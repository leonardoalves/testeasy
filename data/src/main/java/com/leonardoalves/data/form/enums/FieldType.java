package com.leonardoalves.data.form.enums;

/**
 * Created by leonardo on 01/04/2017.
 */

public enum FieldType {
    field("1"),
    text("2"),
    image("3"),
    checkbox("4"),
    send("5");

    private final String name;

    private FieldType(String s) {
        name = s;
    }

    public boolean equalsName(String otherName) {
        return name.equals(otherName);
    }

    public String toString() {
        return this.name;
    }

    public static FieldType fromString(String name) {
        try {
            return findByName(name);
        } catch (Exception e) {
            return null;
        }
    }

    public static FieldType findByName(String name) throws Exception {
        for (FieldType p : values()) {
            if (name.equals(p.toString())) {
                return p;
            }
        }
        return null;
    }
}
