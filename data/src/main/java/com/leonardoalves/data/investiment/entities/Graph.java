package com.leonardoalves.data.investiment.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by leonardo on 02/04/2017.
 */

public class Graph {
    @SerializedName("CDI")
    @Expose
    private List<Double> cDI = null;
    @SerializedName("fund")
    @Expose
    private List<Double> fund = null;
    @SerializedName("x")
    @Expose
    private List<String> x = null;

    public List<Double> getCDI() {
        return cDI;
    }

    public void setCDI(List<Double> cDI) {
        this.cDI = cDI;
    }

    public List<Double> getFund() {
        return fund;
    }

    public void setFund(List<Double> fund) {
        this.fund = fund;
    }

    public List<String> getX() {
        return x;
    }

    public void setX(List<String> x) {
        this.x = x;
    }
}
