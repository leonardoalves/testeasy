package com.leonardoalves.data.investiment.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class _12months {

    @SerializedName("fund")
    @Expose
    private Double fund;
    @SerializedName("CDI")
    @Expose
    private Double cDI;

    public Double getFund() {
        return fund;
    }

    public void setFund(Double fund) {
        this.fund = fund;
    }

    public Double getCDI() {
        return cDI;
    }

    public void setCDI(Double cDI) {
        this.cDI = cDI;
    }

}