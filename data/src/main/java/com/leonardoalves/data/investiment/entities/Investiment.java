package com.leonardoalves.data.investiment.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by leonardo on 02/04/2017.
 */

public class Investiment {
    @SerializedName("screen")
    @Expose
    private Screen screen;

    public Screen getScreen() {
        return screen;
    }

    public void setScreen(Screen screen) {
        this.screen = screen;
    }
}
