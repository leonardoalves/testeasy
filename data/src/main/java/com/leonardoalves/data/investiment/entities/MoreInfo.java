package com.leonardoalves.data.investiment.entities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MoreInfo {

    @SerializedName("month")
    @Expose
    private Month month;
    @SerializedName("year")
    @Expose
    private Year year;
    @SerializedName("12months")
    @Expose
    private _12months _12months;

    public Month getMonth() {
        return month;
    }

    public void setMonth(Month month) {
        this.month = month;
    }

    public Year getYear() {
        return year;
    }

    public void setYear(Year year) {
        this.year = year;
    }

    public _12months get12months() {
        return _12months;
    }

    public void set12months(_12months _12months) {
        this._12months = _12months;
    }

}