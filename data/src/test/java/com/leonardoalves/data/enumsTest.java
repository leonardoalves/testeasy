package com.leonardoalves.data;

import com.leonardoalves.data.form.enums.FieldType;
import com.leonardoalves.data.form.enums.FillTypeField;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class enumsTest {
    @Test
    public void FillTypeField() throws Exception {
        assertEquals(FillTypeField.email, FillTypeField.fromString("3"));
        assertEquals(FillTypeField.text, FillTypeField.fromString("1"));
        assertEquals(FillTypeField.telNumber, FillTypeField.fromString("2"));
    }

    @Test
    public void FieldType() throws Exception {
        assertEquals(FieldType.field, FieldType.fromString("1"));
        assertEquals(FieldType.text, FieldType.fromString("2"));
        assertEquals(FieldType.image, FieldType.fromString("3"));
        assertEquals(FieldType.checkbox, FieldType.fromString("4"));
        assertEquals(FieldType.send, FieldType.fromString("5"));
    }
}