package com.leonardoalves.domain.repository;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.leonardoalves.data.file.LoadJsonFiles;
import com.leonardoalves.data.internet.ApiClient;
import com.leonardoalves.data.internet.ApiInterface;
import com.leonardoalves.data.investiment.entities.Investiment;
import com.leonardoalves.domain.gateway.InvestimetAPIListener;

import java.io.InputStream;
import java.io.InputStreamReader;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by leonardo on 02/04/2017.
 */

public class ScreenRepository {
    private static final String TAG = "GET_INVESTMENT";

    public void getFormServer(final InvestimetAPIListener listener){
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<Investiment> call = apiService.getScreen();
        call.enqueue(new Callback<Investiment>() {
            @Override
            public void onResponse(Call<Investiment>call, Response<Investiment> response) {
                Investiment screen = response.body();
                listener.generateInvestmentScreen(screen);
            }

            @Override
            public void onFailure(Call<Investiment>call, Throwable t) {
                //TODO: Error message
                Log.e(TAG, t.toString());
            }
        });
    }

    public void getFromFile(InvestimetAPIListener listener, Context context){
        LoadJsonFiles loadJsonFiles = new LoadJsonFiles();
        Gson gson = new Gson();
        InputStream fileStream = loadJsonFiles.loadFromFile(context, "fund");
        JsonReader reader = new JsonReader(new InputStreamReader(fileStream));
        Investiment investiment = gson.fromJson(reader, Investiment.class);
        listener.generateInvestmentScreen(investiment);
    }
}
