package com.leonardoalves.domain.repository;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.leonardoalves.data.file.LoadJsonFiles;
import com.leonardoalves.data.form.entities.CellsEntity;
import com.leonardoalves.data.internet.ApiClient;
import com.leonardoalves.data.internet.ApiInterface;
import com.leonardoalves.domain.gateway.CellEntityAPIListener;

import java.io.InputStream;
import java.io.InputStreamReader;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by leonardo on 01/04/2017.
 */

public class FormRepository {
    private static final String TAG = "GET_DATA_FROM_SERVER";

    public void getFormServer(final CellEntityAPIListener listener){
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<CellsEntity> call = apiService.getFields();
        call.enqueue(new Callback<CellsEntity>() {
            @Override
            public void onResponse(Call<CellsEntity>call, Response<CellsEntity> response) {
                CellsEntity cells = response.body();
                listener.generateCells(cells);
            }

            @Override
            public void onFailure(Call<CellsEntity>call, Throwable t) {
                //TODO: Error message
                Log.e(TAG, t.toString());
            }
        });
    }

    public void getFromFile(final CellEntityAPIListener listener, Context context){
        LoadJsonFiles loadJsonFiles = new LoadJsonFiles();
        Gson gson = new Gson();
        InputStream fileStream = loadJsonFiles.loadFromFile(context, "form");
        JsonReader reader = new JsonReader(new InputStreamReader(fileStream));
        CellsEntity cellsEntity = gson.fromJson(reader, CellsEntity.class);
        listener.generateCells(cellsEntity);
    }
}
