package com.leonardoalves.domain.gateway;

import android.view.View;

import com.leonardoalves.data.form.entities.CellsEntity;

import java.util.ArrayList;

/**
 * Created by leonardo on 02/04/2017.
 */

public interface CellEntityAPIListener {
    void generateCells(CellsEntity cellsEntity);
}
