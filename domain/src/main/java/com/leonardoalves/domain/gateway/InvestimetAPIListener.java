package com.leonardoalves.domain.gateway;

import com.leonardoalves.data.investiment.entities.Investiment;

/**
 * Created by leonardo on 02/04/2017.
 */

public interface InvestimetAPIListener {
    void generateInvestmentScreen(Investiment investiment);
}
