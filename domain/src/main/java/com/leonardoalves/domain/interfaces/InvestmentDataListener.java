package com.leonardoalves.domain.interfaces;

import com.leonardoalves.data.investiment.entities.Investiment;

/**
 * Created by leonardo on 02/04/2017.
 */

public interface InvestmentDataListener {
    public void getData(Investiment investiment);
}
