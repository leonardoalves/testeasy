package com.leonardoalves.domain.interfaces;

import android.view.View;

import com.leonardoalves.data.form.entities.CellsEntity;

import java.util.ArrayList;

/**
 * Created by leonardo on 02/04/2017.
 */

public interface FormViewGenerator {
    public ArrayList<View> generateViews(CellsEntity cellsEntity);
}
