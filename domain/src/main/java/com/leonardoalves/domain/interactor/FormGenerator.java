package com.leonardoalves.domain.interactor;

import android.content.Context;

import com.leonardoalves.data.form.entities.CellsEntity;
import com.leonardoalves.data.form.entities.FieldEntity;
import com.leonardoalves.domain.gateway.CellEntityAPIListener;
import com.leonardoalves.domain.interfaces.FormViewGenerator;
import com.leonardoalves.domain.repository.FormRepository;

/**
 * Created by leonardo on 01/04/2017.
 */

public class FormGenerator implements CellEntityAPIListener{
    private  FormViewGenerator formViewGenerator;

    public void generate(FormViewGenerator formViewGenerator, Context context){
        this.formViewGenerator  = formViewGenerator;
        FormRepository repository = new FormRepository();
//        repository.getFormServer(this);
        repository.getFromFile(this, context);
    }

    @Override
    public void generateCells(CellsEntity cellsEntity) {
        formViewGenerator.generateViews(cellsEntity);
    }
}
