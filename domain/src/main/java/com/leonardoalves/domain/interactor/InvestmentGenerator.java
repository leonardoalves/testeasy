package com.leonardoalves.domain.interactor;

import android.content.Context;

import com.leonardoalves.data.investiment.entities.Investiment;
import com.leonardoalves.domain.gateway.InvestimetAPIListener;
import com.leonardoalves.domain.interfaces.InvestmentDataListener;
import com.leonardoalves.domain.repository.ScreenRepository;

/**
 * Created by leonardo on 02/04/2017.
 */

public class InvestmentGenerator implements InvestimetAPIListener {

    private InvestmentDataListener listener;

    public void getInvestmentData(InvestmentDataListener listener, Context context){
        this.listener = listener;
        ScreenRepository repository = new ScreenRepository();
//        repository.getFormServer(this);
        repository.getFromFile(this, context);
    }

    @Override
    public void generateInvestmentScreen(Investiment investiment) {
        listener.getData(investiment);
    }
}
