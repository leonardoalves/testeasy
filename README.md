TestEasy

Esse aplicativo precisa de internet para funcionar pois busca os JSONs do meu servidor pessoal. Caso seja preciso pode-se alterar o BASE_URL do Retrofit no arquivo ApiClient.java

Algumas mudanças visuais foram feitas para deixar o aplicativo com uma melhor conformidade com o Material Design, mas sem perder a identidade visual.
