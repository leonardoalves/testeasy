package com.leonardoalves.testeasy;

import android.app.Fragment;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GridLabelRenderer;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.leonardoalves.data.investiment.entities.DownInfo;
import com.leonardoalves.data.investiment.entities.Graph;
import com.leonardoalves.data.investiment.entities.Info;
import com.leonardoalves.data.investiment.entities.MoreInfo;
import com.leonardoalves.data.investiment.entities.Screen;
import com.leonardoalves.testeasy.presenter.InvestimentPresenter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

/**
 * Created by leonardo on 02/04/2017.
 */

public class Investment extends Fragment{
    @BindView(R.id.loading)
    LinearLayout loading;

    @BindView(R.id.data)
    LinearLayout data;

    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.fundName)
    TextView fundName;

    @BindView(R.id.whatIs)
    TextView whatIs;

    @BindView(R.id.definition)
    TextView definition;

    @BindView(R.id.riskTitle)
    TextView riskTitle;

    @BindView(R.id.graph)
    GraphView graphView;

    @BindView(R.id.infoTitle)
    TextView infoTitle;

    @BindView(R.id.fundMonth)
    TextView fundMonth;
    @BindView(R.id.fundYear)
    TextView fundYear;
    @BindView(R.id.fund12)
    TextView fund12;


    @BindView(R.id.cdiMonth)
    TextView cdiMonth;
    @BindView(R.id.cdiYear)
    TextView cdiYear;
    @BindView(R.id.cdi12)
    TextView cdi12;

    @BindView(R.id.tableInfo)
    TableLayout tableInfo;

    @BindViews({ R.id.risk1, R.id.risk2, R.id.risk3, R.id.risk4, R.id.risk5 })
    List<FrameLayout> riskViews;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.investment_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        InvestimentPresenter presenter = new InvestimentPresenter(this);
        presenter.getViews(getActivity());
    }

    public void setData(Screen data){
        loading.setVisibility(View.GONE);
        this.data.setVisibility(View.VISIBLE);
        this.title.setText(data.getTitle());
        this.fundName.setText(data.getFundName());
        this.whatIs.setText(data.getWhatIs());
        this.definition.setText(data.getDefinition());
        this.riskTitle.setText(data.getRiskTitle());
        this.infoTitle.setText(data.getInfoTitle());
        generateGraph(data.getGraph());
        fillMoreInfo(data.getMoreInfo());
        fillInfo(data.getInfo());
        fillDownInfo(data.getDownInfo());
        fillRisk(data.getRisk());
    }

    private void fillRisk(Integer risk) {
        for (int i = 0; i < riskViews.size(); i++) {
            FrameLayout view = riskViews.get(i);
            if (i + 1 == risk){
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, 40, 1);
                layoutParams.setMargins(1, 0, 1, 0);
                view.setLayoutParams(layoutParams);
            } else {
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                0, 20, 1);
                layoutParams.setMargins(1, 10, 1, 0);
                view.setLayoutParams(layoutParams);
            }
        }
    }

    private void fillMoreInfo(MoreInfo moreInfo) {
        fundMonth.setText(moreInfo.getMonth().getFund().toString()+"%");
        cdiMonth.setText(moreInfo.getMonth().getFund().toString()+"%");

        fund12.setText(moreInfo.get12months().getFund().toString()+"%");
        cdi12.setText(moreInfo.get12months().getFund().toString()+"%");

        fundYear.setText(moreInfo.getYear().getFund().toString()+"%");
        cdiYear.setText(moreInfo.getYear().getFund().toString()+"%");
    }

    private void fillInfo(List<Info> infoList){
        tableInfo.removeAllViews();
        for (Info info : infoList) {
            TableRow row = (TableRow)LayoutInflater.from(getActivity()).inflate(R.layout.attrib_row, null);
            ((TextView)row.findViewById(R.id.attrib_name)).setText(info.getName());
            ((TextView)row.findViewById(R.id.attrib_value)).setText(info.getData());
            tableInfo.addView(row);
        }
    }

    private void fillDownInfo(List<DownInfo> downInfos){

        for (DownInfo info : downInfos) {
            TableRow row = (TableRow)LayoutInflater.from(getActivity()).inflate(R.layout.attrib_down_row, null);
            ((TextView)row.findViewById(R.id.attrib_name)).setText(info.getName());
            ((TextView)row.findViewById(R.id.attrib_value)).setText("Baixar");
            tableInfo.addView(row);
        }
    }
    private void generateGraph(Graph data) {

        LineGraphSeries<DataPoint> cdiSeries = getPoints(data.getCDI().toArray(new Double[0]));
        LineGraphSeries<DataPoint> fund = getPoints(data.getFund().toArray(new Double[0]));
        graphView.addSeries(cdiSeries);

        Paint paint = new Paint();
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(4);
        paint.setColor(getResources().getColor(R.color.blue_300));
        fund.setCustomPaint(paint);
        graphView.addSeries(fund);

        StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graphView);
        staticLabelsFormatter.setHorizontalLabels(data.getX().toArray(new String[0]));
        graphView.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
        graphView.getGridLabelRenderer().setGridStyle(GridLabelRenderer.GridStyle.HORIZONTAL);
        graphView.getGridLabelRenderer().setGridColor(getResources().getColor(R.color.MaterialGrey400));
        graphView.getGridLabelRenderer().setHorizontalLabelsColor(R.color.MaterialGrey300);
        graphView.getGridLabelRenderer().setVerticalLabelsColor(R.color.MaterialGrey300);

    }

    @NonNull
    private LineGraphSeries<DataPoint> getPoints(Double[] cdiValues) {
        ArrayList<DataPoint> cdiDataPoints = new ArrayList<>();
        for (int i = 0; i < cdiValues.length; i++) {
            cdiDataPoints.add(new DataPoint(i, cdiValues[i]));
        }
        LineGraphSeries<DataPoint> series = new LineGraphSeries<>(cdiDataPoints.toArray(new DataPoint[0]));
        series.setDrawDataPoints(false);
        return series;
    }
}
