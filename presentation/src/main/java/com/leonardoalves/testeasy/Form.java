package com.leonardoalves.testeasy;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.leonardoalves.testeasy.presenter.FormPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by leonardo on 02/04/2017.
 */

public class Form extends Fragment {
    @BindView(R.id.loading)
    LinearLayout loading;

    @BindView(R.id.form)
    LinearLayout form;

    @BindView(R.id.success)
    LinearLayout success;

    @BindView(R.id.newMensage)
    Button newMensage;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view  = inflater.inflate(R.layout.form_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        newMensage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                resetFrom();
            }
        });
        FormPresenter presenter = new FormPresenter(this);
        if(form.getChildCount() > 0) {
            form.removeAllViews();
        }
        presenter.getViews();
    }

    public void dismissLoading(){
        loading.setVisibility(View.GONE);
    }

    public void showSuccess() {
        form.setVisibility(View.GONE);
        success.setVisibility(View.VISIBLE);
    }

    public void resetFrom(){
        FormPresenter presenter = new FormPresenter(this);
        if(form.getChildCount() > 0) {
            form.removeAllViews();
        }
        presenter.getViews();
        form.setVisibility(View.VISIBLE);
        success.setVisibility(View.GONE);
    }
}
