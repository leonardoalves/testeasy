package com.leonardoalves.testeasy.presenter;

import android.content.Context;

import com.leonardoalves.data.investiment.entities.Investiment;
import com.leonardoalves.domain.interactor.InvestmentGenerator;
import com.leonardoalves.domain.interfaces.InvestmentDataListener;
import com.leonardoalves.testeasy.Investment;

/**
 * Created by leonardo on 02/04/2017.
 */

public class InvestimentPresenter implements InvestmentDataListener {
    Investment investment;

    public InvestimentPresenter(Investment investment) {
        this.investment = investment;
    }

    public void getViews(Context context){
        InvestmentGenerator investmentGetter = new InvestmentGenerator();
        investmentGetter.getInvestmentData(this, context);
    }

    @Override
    public void getData(Investiment investiment) {
        this.investment.setData(investiment.getScreen());
    }
}
