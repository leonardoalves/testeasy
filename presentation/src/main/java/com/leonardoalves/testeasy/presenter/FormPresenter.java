package com.leonardoalves.testeasy.presenter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;

import com.leonardoalves.data.form.entities.CellsEntity;
import com.leonardoalves.data.form.entities.FieldEntity;
import com.leonardoalves.domain.interactor.FormGenerator;
import com.leonardoalves.domain.interfaces.FormViewGenerator;
import com.leonardoalves.testeasy.Form;
import com.leonardoalves.testeasy.R;
import com.leonardoalves.testeasy.factory.ViewFactory;

import java.util.ArrayList;

/**
 * Created by leonardo on 02/04/2017.
 */

public class FormPresenter implements FormViewGenerator{
    private final Form fragment;
    private final Activity activity;
    ViewFactory viewFactory = new ViewFactory();


    public FormPresenter(Form fragment) {
        this.fragment = fragment;
        this.activity = fragment.getActivity();
    }

    public void getViews(){
        FormGenerator formGenerator = new FormGenerator();
        formGenerator.generate(this, this.activity);
    }

    @Override
    public ArrayList<View> generateViews(CellsEntity cellsEntity) {
        for (FieldEntity fieldEntity : cellsEntity.getCells()) {
            View v = viewFactory.generateView(activity, fieldEntity, fragment);
            ViewGroup layout = (ViewGroup) activity.findViewById(R.id.form);
            layout.addView(v);
        }
        fragment.dismissLoading();
        return null;
    }
}
