package com.leonardoalves.testeasy.factory;

import android.app.Activity;
import android.os.Build;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Pair;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;
import com.leonardoalves.data.form.entities.FieldEntity;
import com.leonardoalves.data.form.enums.FieldType;
import com.leonardoalves.data.form.enums.FillTypeField;
import com.leonardoalves.testeasy.Form;
import com.leonardoalves.testeasy.R;

import java.util.ArrayList;

/**
 * Created by leonardo on 02/04/2017.
 */

public class ViewFactory {
    final ArrayList<Pair<EditText, FieldEntity>> toVerify = new ArrayList<>();

    public View generateView (final Activity context, final FieldEntity fieldEntity, final Form fragment){
        FieldType fieldType = fieldEntity.getViewType();
        switch (fieldType){
            case field:
                return generateEditTextView(context, fieldEntity);
            case text:
                return generateTextView(context, fieldEntity);
            case image:
                return generateImageView(context, fieldEntity);
            case checkbox:
                return generateCheckBox(context, fieldEntity);
            case send:
                return generateSendButton(context, fieldEntity, fragment);
        }
        return null;
    }

    private View generateSendButton(Activity context, FieldEntity fieldEntity, final Form fragment) {
        Button button = new Button(context);
        button.setText(fieldEntity.getMessage());
        button.setTextColor(context.getResources().getColor(R.color.MaterialGrey50));
        button.setBackgroundColor(context.getResources().getColor(R.color.MaterialBlue700));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            button.setElevation(16);
        }
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (Pair<EditText, FieldEntity> editTextFillTypeFieldPair : toVerify) {
                    if (editTextFillTypeFieldPair.second.getRequired()
                            && editTextFillTypeFieldPair.first.getText().toString().trim().isEmpty()
                            && (editTextFillTypeFieldPair.first.isShown())){
                        System.out.println(editTextFillTypeFieldPair.first.getVisibility());
                        editTextFillTypeFieldPair.first.setError("Campo obrigatório");
                        return;
                    }
                    if(editTextFillTypeFieldPair.first.getError()!= null
                            && !editTextFillTypeFieldPair.first.getError().toString().trim().isEmpty()
                            && (editTextFillTypeFieldPair.first.getVisibility() == View.VISIBLE)){
                        return;
                    }
                }
                fragment.showSuccess();
            }
        });
        return setupView(fieldEntity, button);
    }

    private View generateCheckBox(final Activity context, final FieldEntity fieldEntity) {
        CheckBox checkBox = new CheckBox(context);
        checkBox.setText(fieldEntity.getMessage());
        checkBox.setTextColor(context.getResources().getColor(R.color.MaterialGrey500));
        checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckBox checkBox1 = (CheckBox) v;
                if (checkBox1.isChecked()){
                    View view = context.findViewById(1000+Integer.valueOf(fieldEntity.getShow()));
                    view.setVisibility(View.VISIBLE);
                } else {
                    View view = context.findViewById(1000+Integer.valueOf(fieldEntity.getShow()));
                    view.setVisibility(View.GONE);
                }
            }
        });
        return setupView(fieldEntity, checkBox);
    }

    private View generateImageView(Activity context, FieldEntity fieldEntity) {
        ImageView imageView = new ImageView(context);
        Ion.with(imageView)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .animateIn(R.anim.abc_fade_in)
                .load(fieldEntity.getMessage());
        return setupView(fieldEntity, imageView);
    }

    private View generateTextView(Activity context, FieldEntity fieldEntity) {
        TextView textView = new TextView(context);
        textView.setText(fieldEntity.getMessage());
        textView.setTextColor(context.getResources().getColor(R.color.MaterialBlue700));
        return setupView(fieldEntity, textView);
    }

    private View generateEditTextView(Activity context, final FieldEntity fieldEntity) {
        TextInputLayout textInputLayout = new TextInputLayout(context);
        textInputLayout.setHint(fieldEntity.getMessage());
        final EditText editText = new EditText(context);
        editText.setSingleLine();
        editText.addTextChangedListener(new TextWatcher() {
            boolean isUpdating = false;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                if (fieldEntity.getRequired() && s.toString().trim().isEmpty()){
                    editText.setError("Campo obrigatório");
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String preformated = s.toString();
                FillTypeField fillTypeField = fieldEntity.getFillTypeField();
                if (fillTypeField == FillTypeField.telNumber){
                    if (isUpdating) {
                        isUpdating = false;
                        return;
                    }

                    String number = preformated.replaceAll("[^0-9]*", "");
                    if (number.length() > 11)
                        number = number.substring(0, 11);

                    int length = number.length();
                    int cursorPosition = length;
                    for (int i = length; i < 10; i++) {
                        number += " ";
                    }
                    String formatted = "(";
                    cursorPosition++;
                    formatted += number.substring(0,2);
                    formatted += ") ";
                    if (length > 2) {
                        cursorPosition += 2;
                    }
                    if(length > 10) {
                        formatted += number.substring(2, 7);
                        formatted += "-"+ number.substring(7);
                    } else {
                        formatted += number.substring(2, 6);
                        formatted += "-"+ number.substring(6);
                    }
                    if (length > 6){
                        cursorPosition++;
                    }
                    isUpdating = true;
                    editText.setText(formatted);
                    editText.setSelection(cursorPosition);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (fieldEntity.getRequired() && s.toString().trim().isEmpty()){
                    editText.setError("Campo obrigatório");
                }
                FillTypeField fillTypeField = fieldEntity.getFillTypeField();
                switch (fillTypeField) {
                    case text:
                        break;
                    case telNumber:
                        String preformated = s.toString();
                        String number = preformated.replaceAll("[^0-9]*", "");
                        if (number.length()<10){
                            editText.setError("Telefone inválido");
                        }
                        break;
                    case email:
                        if (!isValidEmail(s.toString())){
                            editText.setError("Email inválido");
                        }
                        break;
                }
            }
        });
        toVerify.add(new Pair<>(editText, fieldEntity));
        FillTypeField fillTypeField = fieldEntity.getFillTypeField();
        if (fillTypeField == FillTypeField.telNumber){
            editText.setInputType(InputType.TYPE_CLASS_PHONE);
        }

        if (fillTypeField == FillTypeField.email){
            editText.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        }
        textInputLayout.addView(editText);
        return setupView(fieldEntity, textInputLayout);
    }

    private View setupView(FieldEntity fieldEntity, View view) {
        view.setId(1000+fieldEntity.getId());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, fieldEntity.getTopSpacing().intValue(), 0, 0);
        view.setLayoutParams(layoutParams);
        if (fieldEntity.getHidden()){
            view.setVisibility(View.GONE);
        }
        return view;
    }

    public final static boolean isValidEmail(String target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
}
